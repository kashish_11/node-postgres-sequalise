1) To have PostgresSQL download it according to the operating system from "https://www.postgresql.org/download/"

2) Now install it.

3) To have a UI for postgres download pgadmin from "https://www.pgadmin.org/download/" and install it.

4) Then create the username and password for pgadmin and login.And in cmd run command "psql -U postgres" to run postgres commands and then create db using command "create database db_name"

5) Install sequalise,pg,pg-hstore and then run command "sequelise init" to initialise sequelise which will create config file.

6) In the config file change the values of username,password,database and dialect according to the specification.Here database 

7) Now your db is connected and can create model for example "sequelize model:create --name Todo --attributes title:string"

8) now migrate the changes using the command "sequelize db:migrate".

9) Now we can perform operations and application is ready to run using command npm run start.


Now the changes made due to upgradation to new versions are:-

1)In controllers file function findById() is now findByPk().

2)Now we use class for defining models earlier we used constants.

For Referance see link "https://scotch.io/tutorials/getting-started-with-node-express-and-postgres-using-sequelize"



